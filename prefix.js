const operators = {
  "+": (a, b) => a + b,
  "-": (a, b) => a - b,
  "*": (a, b) => a * b,
  "/": (a, b) => Math.floor(a / b),
};

const isOperand = (input) => /^-?\d+$/.test(input) && parseInt(input) > 0;

const isOperator = (input) => operators[input];

const makeExressionReady = (expression) =>
  expression.trim().replace(/  +/g, " ").split(" ").reverse();

const result_expression = (expression, variables) => {
  const stack = [];

  try {
    makeExressionReady(expression).forEach((e) => {
      if (isOperand(e)) stack.push(parseInt(e));
      else if (isOperator(e)) {
        const op1 = stack.pop();
        const op2 = stack.pop();
        if (!op1 || !op2) throw new Error("the operator needs 2 operand.");
        stack.push(operators[e](op1, op2));
      } else stack.push(variables[e]);
    });
    return stack.length === 1 ? stack[0] : null;
  } catch (e) {
    return null;
  }
};
